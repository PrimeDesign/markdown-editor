const ipc = require('electron').ipcRenderer

// buttons
const newfileBtn = document.getElementById('newfile')
const fileBtn = document.getElementById('openfile')
const saveBtn = document.getElementById('savefile')
const previewBtn = document.getElementById('preview')
const publishBtn = document.getElementById('publish')
const helpBtn = document.getElementById('help')
const settingBtn = document.getElementById('setting')

ipc.send('check-varibles')

// add listner
helpBtn.addEventListener('click', function () {
  ipc.send('help-dialog')
})

publishBtn.addEventListener('click', function () {
  ipc.send('publish-dialog')
})

previewBtn.addEventListener('click', function () {
  var currentdata = document.getElementsByClassName('ql-editor')[0].innerHTML
  ipc.send('preview-dialog', currentdata)
})

newfileBtn.addEventListener('click', function () {
  ipc.send('new-file-dialog')
})

fileBtn.addEventListener('click', function () {
  ipc.send('open-file-dialog')
})

saveBtn.addEventListener('click', function () {
  var currentdata = document.getElementsByClassName('ql-editor')[0].innerHTML
  ipc.send('save-file-dialog', currentdata)
})

settingBtn.addEventListener('click', function () {
  ipc.send('setting-dialog')
})

// reciever
ipc.on('selected-directory', function (event, path) {
  document.getElementById('selected-file').innerHTML = `You selected: ${path}`
})

ipc.on('selected-file', function (event, result) {
  quill.clipboard.dangerouslyPasteHTML(result)
})
