const {
  app,
  BrowserWindow
} = require('electron')
const path = require('path')
const url = require('url')
const ipc = require('electron').ipcMain
const dialog = require('electron').dialog
const os = require('os')
// markdown to html
const showdown = require('showdown')
const converter = new showdown.Converter()
// file
const fs = require('fs')
// html to markdown
const tomarkdown = require('to-markdown')
// git
const simplegit = require('simple-git')
// json
const json = require('jsonfile')
// logging
const pino = require('pino')()

pino.info('Running in : ' + os.type())
// varibles
var varibles = jsonread()

// checks
ipc.on('check-varibles', function (event) {
  if (varibles != null) {
    if (varibles.filepath != null) {
      pino.info('Varibles: %s', varibles)
      // read file
      fs.readFile(varibles.filepath, 'utf-8', (err, data) => {
        if (err) {
          dialog.showErrorBox('An error ocurred reading the file : ' + err.message)
          pino.error('An error ocurred reading the file: %s', err)
        } else {
          // convert file to html
          var result = markdowntohtml(data)

          pino.info('markdowntohtml: %s', result)

          event.sender.send('selected-file', result)
        }
      })
    }
  }
})

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win
let winpreview
let winsetting
let winhelp
let options = {
  width: 1100,
  height: 1000,
  icon: path.resolve(__dirname, 'icon.jpg'),
  webPreferences: {
    devTools: true
  }
}

function createWindow () {
  // Create the browser window.
  win = new BrowserWindow(options)

  win.setMenu(null)
  if (varibles.debug) {
    win.webContents.openDevTools({
      mode: 'detach'
    })
  }

  // and load the index.html of the app.
  win.loadURL(url.format({
    pathname: path.resolve(__dirname, 'app', 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  jsonwrite(varibles, path.resolve(__dirname, 'app', 'data.json'))
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

ipc.on('open-file-dialog', function (event) {
  dialog.showOpenDialog({
    filters: [{
      name: 'Markdown Files',
      extensions: ['md']
    }],
    properties: ['openFile']
  }, function (files) {
    if (files) {
      varibles.filepath = files[0]

      varibles.folderpath = path.dirname(varibles.filepath)
      pull(varibles.folderpath)
      // read file
      fs.readFile(varibles.filepath, 'utf-8', (err, data) => {
        if (err) {
          dialog.showErrorBox('Error Reading', 'An error ocurred reading the file : ' + err.message)

          pino.error('An error ocurred reading the file: %s', err)
        } else {
          // convert file to html
          var result = markdowntohtml(data)

          pino.info('markdowntohtml: %s', result)

          event.sender.send('selected-file', result)
        }
      })
    }
  })
})

ipc.on('new-file-dialog', function (event) {
  dialog.showSaveDialog({
    filters: [{
      name: 'Markdown',
      extensions: ['md']
    }]
  },
    function (fileName) {
      if (fileName === undefined) {
        console.log("You didn't name the file")
      } else {
        // get template
        fs.readFile(path.resolve(__dirname, 'app', 'default.md'), 'utf-8', (err, data) => {
          if (err) {
            dialog.showErrorBox('Error Reading Newly Created File', 'An error ocurred reading the file : ' + err.message)
            return
          }
          varibles.filepath = fileName
          varibles.folderpath = path.dirname(fileName)
          pull(varibles.folderpath)

          // fileName is a string that contains the path and filename created in the save file dialog
          fs.writeFile(fileName, data, (err) => {
            if (err) {
              pino.error('Error ocurred creating the file: %s', err)
              dialog.showErrorBox('An error ocurred creating the file', err.message)
            }
          })

          const options = {
            type: 'info',
            title: 'Save successful',
            message: 'New File Created',
            buttons: ['Cool']
          }

          dialog.showMessageBox(options, function (index) {})

          var result = markdowntohtml(data)

          event.sender.send('selected-file', result)
        })
      }
    })
})

ipc.on('save-file-dialog', function (event, content) {
  var result = htmltomarkdown(content)

  pino.info('htmltomarkdown: %s', result)

  fs.writeFile(varibles.filepath, result, (err) => {
    if (err) {
      dialog.showErrorBox('An error ocurred creating the file: ', err.message)
    }
  })

  varibles.folderpath = path.dirname(varibles.filepath)
  add(varibles.folderpath)
})

ipc.on('preview-dialog', function (event, currentdata) {
  // Create the browser window.
  winpreview = new BrowserWindow({
    width: 1100,
    height: 1000,
    icon: path.resolve(__dirname, 'icon.jpg'),
    webPreferences: {
      devTools: true
    }
  })
  winpreview.setMenu(null)
  // and load the index.html of the app.
  winpreview.loadURL(url.format({
    pathname: path.resolve(__dirname, 'app', 'preview', 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Emitted when the window is closed.
  winpreview.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null
  })
  winpreview.webContents.on('did-finish-load', () => {
    winpreview.webContents.send('preview-currentdata', currentdata)
  })
})

ipc.on('help-dialog', function (event) {
  winhelp = new BrowserWindow({
    width: 900,
    height: 700,
    webPreferences: {
      nativeWindowOpen: true
    }
  })

  winhelp.loadURL('https://primedesign.gitlab.io/help/')
})

ipc.on('setting-dialog', function (event) {
  // Create the browser window.
  winsetting = new BrowserWindow({
    width: 1100,
    height: 1000,
    icon: path.resolve(__dirname, 'icon.jpg'),
    webPreferences: {
      devTools: true
    }
  })
  winsetting.setMenu(null)
  // and load the index.html of the app.
  winsetting.loadURL(url.format({
    pathname: path.resolve(__dirname, 'app', 'settings', 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  // Emitted when the window is closed.
  winsetting.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null
  })
  winsetting.webContents.on('did-finish-load', () => {
    winsetting.webContents.send('varibles', varibles)
  })
})

ipc.on('publish-dialog', function (event) {
  if (varibles.filepath !== null) {
    var onlyPath = path.dirname(varibles.filepath)
    push(onlyPath)
  }
})

function push (dir) {
  pino.info('Starting Push..')
  var filename = path.basename(varibles.filepath)

  simplegit(dir)
    .pull()
    .commit(filename + ' - Editor')
    .push('origin', 'master', function (err) {
      if (err) {
        pino.error('Error Push: %s', err)
      }
    })

  pino.info('Push Done..')
}

function add (dir) {
  var filename = path.basename(varibles.filepath)
  simplegit(dir)
    .add(path.resolve(dir, filename))
}

function pull (dir) {
  pino.info('Starting Pull..')

  simplegit(dir)
    .pull(function (err) {
      if (err) {
        pino.error('Error Pull: %s', err)
      }
    })

  pino.info('Pull Done..')
}

// every 5 minutes
setInterval(
  function () {
    pull(varibles.folderpath)
  }
  , 300000)

function htmltomarkdown (content) {
  var converter1 = {
    filter: 'hr',
    replacement: function (content) {
      return '---'
    }
  }
  var result = tomarkdown(content, {
    converters: [converter1]
  })
  return result
}

function markdowntohtml (data) {
  var result = converter.makeHtml(data)
  return result
}

function jsonwrite (obj, path) {
  json.writeFileSync(path, obj, {
    spaces: 2
  })
}

function jsonread () {
  var obj = json.readFileSync(path.resolve(__dirname, 'app', 'data.json'), function (err, obj) {
    if (err) {
      pino.error('An error ocurred reading json file: %s', err)
      return
    }
    console.dir(obj)
    return obj
  })
  return obj
}
