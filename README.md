## Description
[![license](https://img.shields.io/badge/Licence-MIT-blue.svg?style=flat-square)]()
[![standard](https://img.shields.io/badge/Style-Standard-green.svg?style=flat-square)]()

This project is a Markdown Editor using electron. The reason for this project was because the team i have in a project doesn't wish to create blog posts using markdown, so this project came to be.

The project uses npm packages:

* "markdown-it"
* "marked"
* "quill"
* "simple-git"
* "to-markdown"

## Build

1. Download the project
2. In the terminal run 'npm install'
3. In the terminal run 'npm start'

** As far as it seems i cant add prebuilt files in the tags section (to large) **